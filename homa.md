# Linux Server

- IP: 115.79.198.71:2322; 14.161.48.60:2322
- Accounts: cuongdh/Homa@581; voices/Homa@seciov

# Voice Commands training

1. Local Vostro 5568

```
# make sure tensorflow is installed
cd $HOME/workspace/home/voice_opensource/tensorflow

# create virtualenv if not existed
virtualenv -p python3 venv --system-site-packages

# active environment
source venv/bin/activate
export DATADIR=$HOME/workspace/data/ml_data/speech_commands

# train command
optirun python tensorflow/examples/speech_commands/train.py --data_dir $DATADIR --train_dir $(pwd)/train_dir --summaries_dir $(pwd)/tensorboard_dir
```

2. On HOMA server
```
ssh voices@115.79.198.71 -p 2322 # passwd: Homa@seciov

```

# Install jupyter notebook on server
