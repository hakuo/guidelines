# Nvidia GPU installation guide


## Install nvidia driver
```
sudo apt-get install nvidia-384 nvidia-modprobe
sudo apt-get install nvidia-current
```

## Install CUDA 9.0
```
cd && wget https://developer.nvidia.com/compute/cuda/9.0/Prod/local_installers/cuda_9.0.176_384.81_linux-run
```

We will have:
1. an NVIDIA driver installer, but usually of stable version `NVIDIA-Linux-x86_64-384.81.run`
2. the actual CUDA installer `cuda-linux.9.0.176-22781540.run`
3. the CUDA samples installer `cuda-samples.9.0.176-22781540-linux.run`

```
# install CUDA and samples
sudo ./cuda-linux.9.0.176-22781540.run
sudo ./cuda-samples.9.0.176-22781540-linux.run
```
 After installation finishes, configure the runtime library
 ```
 sudo bash -c "echo /usr/local/cuda/lib64/ > /etc/ld.so.conf.d/cuda.conf"
 sudo ldconfig

 sudo vim /etc/environment
 # add to path variable: ':/usr/local/cuda/bin'

 # reboot
 ```

 Verify the installation by invoking our tests:
 ```
 cd /usr/local/cuda-9.0/samples && sudo make
 cd ./bin/x86_64/linux/release
 ./deviceQuery
 ```

 Result:
 ```
 ./deviceQuery Starting...
 CUDA Device Query (Runtime API) version (CUDART static linking)
Detected 1 CUDA Capable device(s)
Device 0: "GeForce GTX 1060"
  CUDA Driver Version / Runtime Version          9.0 / 9.0
  CUDA Capability Major/Minor version number:    6.1
  Total amount of global memory:                 6073 MBytes (6367739904 bytes)
  (10) Multiprocessors, (128) CUDA Cores/MP:     1280 CUDA Cores
  GPU Max Clock rate:                            1671 MHz (1.67 GHz)
  Memory Clock rate:                             4004 Mhz
  Memory Bus Width:                              192-bit
  L2 Cache Size:                                 1572864 bytes
  Maximum Texture Dimension Size (x,y,z)         1D=(131072), 2D=(131072, 65536), 3D=(16384, 16384, 16384)
  Maximum Layered 1D Texture Size, (num) layers  1D=(32768), 2048 layers
  Maximum Layered 2D Texture Size, (num) layers  2D=(32768, 32768), 2048 layers
  Total amount of constant memory:               65536 bytes
  Total amount of shared memory per block:       49152 bytes
  Total number of registers available per block: 65536
  Warp size:                                     32
  Maximum number of threads per multiprocessor:  2048
  Maximum number of threads per block:           1024
  Max dimension size of a thread block (x,y,z): (1024, 1024, 64)
  Max dimension size of a grid size    (x,y,z): (2147483647, 65535, 65535)
  Maximum memory pitch:                          2147483647 bytes
  Texture alignment:                             512 bytes
  Concurrent copy and kernel execution:          Yes with 2 copy engine(s)
  Run time limit on kernels:                     Yes
  Integrated GPU sharing Host Memory:            No
  Support host page-locked memory mapping:       Yes
  Alignment requirement for Surfaces:            Yes
  Device has ECC support:                        Disabled
  Device supports Unified Addressing (UVA):      Yes
  Supports Cooperative Kernel Launch:            Yes
  Supports MultiDevice Co-op Kernel Launch:      Yes
  Device PCI Domain ID / Bus ID / location ID:   0 / 1 / 0
  Compute Mode:
     < Default (multiple host threads can use ::cudaSetDevice() with device simultaneously) >
deviceQuery, CUDA Driver = CUDART, CUDA Driver Version = 9.0, CUDA Runtime Version = 9.0, NumDevs = 1
Result = PASS
 ```
* HOTFIX 1: cudaGetDeviceCount returned 30 (modprobe: FATAL: Module nvidia-uvm not found in directory /lib/modules/4.15.0-39-generic)
  ```
  # Find module nvidia-uvm
  locate --regex nvidia.*uvm.ko
  # Create and save 'alias nvidia-uvm nvidia_384_uvm' on
  sudo vim /etc/modprobe.d/bumblebee.conf 
  # Update db
  sudo updatedb
  ```
* HOTFIX 2: Ubuntu 16.04 LTS Running low-graphics mode
  ```
  # Ctrl + Alt + F1
  # Reset the "lightdm"
  sudo rm /etc/lightdm/lightdm.conf
  sudo ucf -p /etc/lightdm/lightdm.conf
  sudo apt-get install --reinstall lightdm

  # Reinstall driver
  sudo ubuntu-drivers devices 
  sudo apt-get install nvidia-384  

  # Reboot system
  ```

 ## Install cuDNN 7.0
1. Go to [cuDNN download page](https://developer.nvidia.com/rdp/cudnn-download) and select the lastest `cuDNN` version made for `CUDA 9.0`
2. Download `deb` files: the runtime library, the developer library, and the code samples library for Ubuntu 16.04
3. Install them in the sample order:
   
```
sudo dpkg -i libcudnn7_7.0.5.15–1+cuda9.0_amd64.deb # the runtime library

sudo dpkg -i libcudnn7-dev_7.0.5.15–1+cuda9.0_amd64.deb # the developer library

sudo dpkg -i libcudnn7-doc_7.0.5.15–1+cuda9.0_amd64.deb # the code samples
``` 

Check your installation
```
cp -r /usr/src/cudnn_samples_v7/ ~
cd ~/cudnn_samples_v7/mnistCUDNN
make clean && make
./mnistCUDNN # Expected: Test passed!
```

Configure the `CUDA` and `cuDNN` library paths:
```
# put the following line in the end or your .bashrc file
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/local/cuda/extras/CUPTI/lib64"
```