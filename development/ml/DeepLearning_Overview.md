# TensorFlow

TensorFlow is build around a concept of [Static Computational Graph (SCG)](). That means, first you define everything that is going to happen inside your framework, then you run it. It has two great upsides:

1. When a model becomes obscenely huge, it's easier to understand it, because everything is basically a giant function that never changes.
2. It's always easier to optimize a static computational graph, because it allows all kinds of tricks: preallocating buffers, fusing layers, precompiling the functions.

TensorFlow has very wide support for parallel and distributed training. If you have 100 GPUs... well, stop wasting time here and go check your networks, something must be already finished.

# PyTorch

A network written in Pytorch is a [Dynamic Computational Graph (DCG)](). It allows you to do any crazy thing you want to do.

1. Dynamic data structures inside the network. You can have any number of inputs at any given point of training in PyTorch. Lists? Stacks? No problem!
2. Networks are modular. Each part is implemented seperately, and you can debug it separately, unlike a monolithic TF construction.

# Caffe

# Keras

