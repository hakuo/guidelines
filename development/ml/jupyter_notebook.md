# Jupyter notebook 

## Presiquisites

- python3
- python-pip
- jupyter
- openssl

## Setup

```
# generate configuration file
jupyter notebook --generate-config

# generate ssl certificate
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout mykey.key -out mycert.pem

# modify configuration file with the following options
vim ~/.jupyter/jupyter_notebook_config.py

# c.NotebookApp.port = 2522
# c.NotebookApp.ip = '0.0.0.0' # listen all ip
# c.NotebookApp.open_browser = False
# c.NotebookApp.certfile = ''
# c.NotebookApp.keyfile = ''

# run jupyter notebook 
jupyter notebook
```