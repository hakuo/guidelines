# Basic Machine Learning
## Learning resource
- Machine Learning cơ bản - TiepVu
- Machine Learning Yearning - Andew Ng.
- Machine Learning: An probabilistic perspective - Kevin P. Murphy
- [fast.ai courses](course.fast.ai)
- [Machine Learning in Action](https://github.com/pbharrin/machinelearninginaction)
- [Resource for Machine Learning](https://github.com/dvrg/educate-resource-for-machine-learning)
- [Awesome Tensorflow](https://github.com/jtoy/awesome-tensorflow)
- [Hvass-Labs: Tensorflow tutorial](https://github.com/Hvass-Labs/TensorFlow-Tutorials)
- [Effective TensorFlow](https://github.com/vahidk/EffectiveTensorflow)
- [Kaggle: Machine Learning tutorial for beginners](https://www.kaggle.com/kanncaa1/machine-learning-tutorial-for-beginners)
- [Machine Learning Algorithms from Scratch](https://github.com/madhug-nadig/Machine-Learning-Algorithms-from-Scratch)
- [Standford UFLDL Tutorial](http://deeplearning.stanford.edu/tutorial/)
- [Neural network - 3Blue1Brown](https://www.youtube.com/playlist?list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi)

## Demo
- [Support Vector Machine in Javascript](https://cs.stanford.edu/~karpathy/svmjs/demo/)
- [Neural Networks demo in Javascript](https://cs.stanford.edu/~karpathy/svmjs/demo/demonn.html)
- [Random Forest demo in Javascript](https://cs.stanford.edu/~karpathy/svmjs/demo/demoforest.html)

# Automatic Speech Recognition (ASR)
- [End-to-end Automatic Speech Recognition for Madarian and English in Tensorflow - zzw922cn](https://github.com/zzw922cn/Automatic_Speech_Recognition)
- [Keras: Conv-LSTM-CTC speech recognition network (end-to-end), written in TensorFlow](https://github.com/huschen/kaggle_speech_recognition)
- [Keras: Speech Command Recognition](https://github.com/douglas125/SpeechCmdRecognition) 
- [Kaggle: Tensorflow Speech Recognition Challenge Solution](https://github.com/heyt0ny/Kaggle-TensorFlow-Speech-Recognition-Challenge-Solution)
- [Papers about Automatic Speech Recognition, Speaker Verification, Speech Synthesis, Language Modeling](https://github.com/zzw922cn/awesome-speech-recognition-speech-synthesis-papers)

# Computer Vision
## Learning Resource
- [Standford EE368/CS232: Digital Image Processing - Prof. Bernd Girod](https://web.stanford.edu/class/ee368/)

## Object Detection
- [DarkFlow: Real-time object detection and classification](https://github.com/thtrieu/darkflow)
- [QuickDraw](https://github.com/1991viet/QuickDraw)

## Face detection
- [DeepFace](https://github.com/noorkhokhar99/DeepFace)

# Scholarship
- [Toulouse Fellowship](https://sites.google.com/site/torusfoundation/website-builder)

