# Install development tools

```
# C/C++ development: QtCreator
sudo apt-get install vim git meld gcc g++ cmake make libtool m4

# Python development: anaconda, PyCharm

# Java development: JDK, Eclipse, JavaFX Scene

# Android development: Android Studio

# Computer Vision: OpenCV

# Machine Learning: Tensorflow, Keras

# Other tools: VSCode, Synergy 1.8.8
```

# Tips

## Fix time differences in Ubuntu 16.04 & Windows 10 Dual Boot

```
# 
# Disable UTC and use Local Time in Ubuntu
timedatectl set-local-rtc 1 --adjust-system-clock

# To check out if your system uses Local time
timedatectl
```

Or

```
# Make sure Ubuntu uses UTC
timedatectl set-local-rtc 0

# Make Windows uses UTC: Run 'cmd' as Adminstrator
# Windows x86
Reg add HKLM\SYSTEM\CurrentControlSet\Control\TimeZoneInformation /v RealTimeIsUniversal /t REG_DWORD /d 1
# Windows x64
Reg add HKLM\SYSTEM\CurrentControlSet\Control\TimeZoneInformation /v RealTimeIsUniversal /t REG_QWORD /d 1
```

## Setup synergy start when startup
```
cp /usr/share/applications/synergy.desktop ~/.config/autostart/
chmod 664 ~/.config/autostart/synergy.desktop
```
Or using below command to start synergy before login

```
sudo cp /usr/share/applications/synergy.desktop /etc/xdg/autostart/
sudo chmod 644 /etc/xdg/autostart/synergy.desktop  
```