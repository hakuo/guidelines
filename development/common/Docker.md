# Docker

## Setup

### On Ubuntu
```
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# add docker repository
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# install docker-ce
sudo apt-get update
sudo apt-get install docker-ce
sudo docker run hello-world
```

## Common docker

```
# add user to docker group to eliminate `sudo`
sudo usermod -a -G docker $USER

# run docker daemon 
sudo systemctl start docker

# if you want docker daemon start on boot
sudo systemctl enable docker
```

## Docker container

```
# start docker container in interactive mode
docker container start -i <docker_id>
```
