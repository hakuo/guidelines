#!/bin/bash

if [ ! -d /run/media/data ]; then
  echo "/run/media/data is not existed, create a new one"
  mkdir -p /run/media/data
  mount -t auto -v /dev/sda2 /run/media/data
fi

if [ ! -L $(pwd)/data ]; then
  echo "Symbolic link $(pwd)/data is not existed, create a new one"
  ln -s /run/media/data $(pwd)/data
fi
